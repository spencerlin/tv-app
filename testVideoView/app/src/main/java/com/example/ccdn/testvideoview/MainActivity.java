package com.example.ccdn.testvideoview;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {

    final int goToMain = 1;
    final int goToPlayTime = 2;
    Timer timer = null;
    //直播時段:早上六點~早上十點,晚上八點~晚上十點，將時段內的小時存入陣列
    ArrayList<Integer> playTime = new ArrayList(Arrays.asList(6,7,8,9,20,21,17));

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //只在首播和重播時段進入播放頁面
        checkTime();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        task.cancel();
        timer.cancel();
        if(task!=null){
            task = null;
        }
        if(timer!=null){
            timer = null;
        }

    }

    // 設定 Layout
    public void setLayout(final int layoutNum)
    {
        // 設定 Layout 與找出 Button 物件
        if( layoutNum == 1 )
        {
            Log.d("spencer","activity_main");
            setContentView(R.layout.activity_main);
            TextView playVideo = (TextView)findViewById(R.id.playVideo);
            //測試導時間頁面用
            playVideo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    setLayout( goToPlayTime );
                }
            });
            ImageView playImage = (ImageView)findViewById(R.id.playImage);
            playImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(MainActivity.this,VideoActivity.class);
                    MainActivity.this.startActivity ( intent );
                }
            });
            //youtube超連結
            TextView youtubeLink = (TextView)findViewById(R.id.YouTubeLink);
            youtubeLink.setMovementMethod(LinkMovementMethod.getInstance());

        }
        else
        {
            Log.d("spencer","activity_play_time");
            setContentView(R.layout.activity_play_time);
            //youtube超連結
            TextView youtubeLink = (TextView)findViewById(R.id.YouTubeLink);
            youtubeLink.setMovementMethod(LinkMovementMethod.getInstance());
        }


    }

    //在直播和重播時段才進入主畫面，其他時間一律進入時間頁面
    private void checkTime(){
        //先建立Timer物件，下面不同時段call不同的task
        timer = new Timer();

        Calendar c = Calendar.getInstance();
        int currentHour = c.get(Calendar.HOUR_OF_DAY);


        timer.schedule(task,0,1000);




    }

    //判斷目前時間如果在直播(重播)時段內就導入播放頁面，在時段外則導入時間頁面
    private TimerTask task = new TimerTask(){
        @Override
        public void run() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Calendar c = Calendar.getInstance();
                    int currentHour = c.get(Calendar.HOUR_OF_DAY);
                    int currentMin = c.get(Calendar.MINUTE);
                    int currentSec = c.get(Calendar.SECOND);
                    Log.d("spencer","task2:"+currentHour+":"+currentMin+":"+currentSec);

                    if(playTime.contains(currentHour)){
                        setLayout( goToMain );
                    }else{
                        setLayout( goToPlayTime );
                    }


                }
            });

        }
    };




}
