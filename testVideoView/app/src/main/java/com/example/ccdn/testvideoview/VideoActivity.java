package com.example.ccdn.testvideoview;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.MediaController;

import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Spencer on 2017/1/10.
 */

public class VideoActivity extends AppCompatActivity implements VideoActivityDelegate{

    private static final int MESSAGE_RTSP_OK = 1;
    private static final int MESSAGE_RTSP_ERROR = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);

        final MyVideoView videoView = (MyVideoView)findViewById(R.id.video_view);
        //判斷是否有網路
        if(haveInternet()){
            CheckingSiteResponse task = new CheckingSiteResponse("http://flv.ccdntech.com/live/_definst_/vod252_Live/live/playlist.m3u8",VideoActivity.this,VideoActivity.this,videoView);
            task.execute();
        }else{
            AlertDialog.Builder dialog = new AlertDialog.Builder(VideoActivity.this);
            dialog.setTitle("訊息");
            dialog.setMessage("未偵測到網路");
            dialog.setPositiveButton("返回",new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface arg0, int arg1) {
                    // TODO Auto-generated method stub
                    VideoActivity.this.finish();
                }});
            dialog.show();
        }


    }

    //偵測網路連線
    private boolean haveInternet()
    {
        boolean result = false;
        ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info=connManager.getActiveNetworkInfo();
        if (info == null || !info.isConnected())
        {
            result = false;
        }
        else
        {
            if (!info.isAvailable())
            {
                result =false;
            }
            else
            {
                result = true;
            }
        }
        return result;
    }


    @Override
    public void closeVideoActivity() {
        VideoActivity.this.finish();
    }
}


class CheckingSiteResponse extends AsyncTask<Void, Void, Boolean> {

    private final String mUrl;

    private MyVideoView mVideoView = null;
    public int responseCode = 0;
    Boolean httpConnectResponse = false;
    private Context mContext;
    private VideoActivityDelegate mVideoDelegate ;
    ProgressDialog mypDialog=null;


    CheckingSiteResponse(String url,VideoActivityDelegate delegate,Context context,MyVideoView videoView) {
        mUrl = url;
        mVideoDelegate = delegate;
        mVideoView = videoView;
        mContext = context;
    }

    @Override
    protected Boolean doInBackground(Void... params) {

        try {
            HttpURLConnection connection = (HttpURLConnection) new URL(mUrl).openConnection();
            connection.setConnectTimeout(15000);
            connection.setReadTimeout(15000);
            connection.setRequestMethod("HEAD");
            responseCode = connection.getResponseCode();
            httpConnectResponse = (200 >= responseCode && responseCode <= 399);

            Log.d("TAG", "RESPONCE = " + responseCode);


        } catch (Exception e) {
            e.printStackTrace();
        }

        return httpConnectResponse;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        mypDialog = ProgressDialog.show(mContext,
                "讀取中", "請稍後...",true);
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);
        mypDialog.dismiss();
        //訊號源回應狀態是否正常
        if(httpConnectResponse){
//            Uri uri = Uri.parse("rtsp://wowzaec2demo.streamlock.net/vod/mp4:BigBuckBunny_115k.mov");
//            Uri uri = Uri.parse("rtsp://cws4.ccdntech.com/live/spencer");
            Uri uri = Uri.parse("http://flv.ccdntech.com/live/_definst_/vod252_Live/live/playlist.m3u8");

            mVideoView.setMediaController(new MediaController(mContext));
            mVideoView.setVideoURI(uri);
            mVideoView.requestFocus();
            //監聽無法播放影片事件
            mVideoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                @Override
                public boolean onError(MediaPlayer mediaPlayer, int i, int i1) {
                    Log.d("onError", String.valueOf(i));
                    return false;
                }
            });
            mVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mediaPlayer) {
                    mVideoView.start();
//                    videoView.requestFocus();
                }
            });



            mVideoView.setOnInfoListener(new MediaPlayer.OnInfoListener() {
                @Override
                public boolean onInfo(MediaPlayer mediaPlayer, int i, int i1) {
                    Log.d("spencer-video", String.valueOf(i));
                    return false;
                }
            });
            mVideoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mediaPlayer) {
                    Log.d("spencer-onCompletion","Completion");
                }
            });
        }else{
            AlertDialog.Builder dialog = new AlertDialog.Builder(mContext);
            dialog.setTitle("訊息");
            dialog.setMessage("目前直播串流無訊號請稍候再試。");
            dialog.setPositiveButton("返回",new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface arg0, int arg1) {
                    // TODO Auto-generated method stub
                    arg0.cancel();
                    mVideoDelegate.closeVideoActivity();
                }});
            dialog.show();

        }


    }
}
